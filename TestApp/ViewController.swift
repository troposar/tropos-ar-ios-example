//
//  ViewController.swift
//  MyApp
//
//  Created by Vesna Strezoska on 4.10.21.
//

import UIKit
import SwiftUI
import TroposARSDK

class ViewController: UIViewController {
   
    @IBOutlet weak var btnStart : UIButton!
    var _window: UIWindow?
    private var tarView = UIView();
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        _window = self.view.window
        TARView.setHostMainWindow(_window)
    }
    
    
    @IBAction func btnStartClicked(_ sender: Any) {
        do {
            //Initialize the Tropos SDK to directly load an asset after displaying the Tropos AR view
            try TARView.initTAR(_assetId: "6193861cbac025c7b87c43ad")
            //Initialize the Tropos SDK to display the Tropos AR view
            try TARView.initTAR()
            //Show the Tropos AR view
            TARView.showTARView()
        
        } catch TARError.apiKeyUnset {
            print("API key is not assigned");
        } catch {
            print("Unexpected error: \(error).")
        }
    }
}

